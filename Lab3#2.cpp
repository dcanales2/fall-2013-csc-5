#include <cstdlib>
#include <iostream>

//Daniel Canales
//2449175
//9/11/13
//Hw: Week 3 #2
//Is it correct?
//I certify this is my own work and code

using namespace std;


int main(int argc, char *argv[])
{
    int singles;
    int doubles;
    int triples;
    int homeruns;
    int atbat;
    float slughit;
    
    cout << "Enter the number of times AT BAT" << endl;
    cin >> atbat;
    
    cout << "Enter the number of SINGLES" << endl;
    cin >> singles;
    
    cout << "Enter the number of DOUBLES" << endl;
    cin >> doubles;
    
    cout << "Enter the number of TRIPLES" << endl;
    cin >> triples;
    
    cout << "Enter the number of homeruns" << endl;
    cin >> homeruns;
    
    slughit = ((singles+2)*(doubles+3)*(triples+4)*homeruns)/atbat;
   
    cout << "Slugging Percentage is " << slughit << endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
