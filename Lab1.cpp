#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    string name;
    cout << "Hello, my name is Hal!" << endl;
    cout << "What is your name?" << endl;
    cin >> name;
    cout << "Hello " << name << ". I am glad to meet you." << endl;
    system("PAUSE");
    return EXIT_SUCCESS;
}
