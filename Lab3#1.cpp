#include <cstdlib>
#include <iostream>
//Daniel Canales
//2449175
// 9/11/13
// HW: Lab 3
// I cant switch Y to become X 
// I certify that this is my own work and code
using namespace std;

int main(int argc, char *argv[])
{   
    int x;
    int y;
    cout << "Enter a number between 0-1000" << endl;
    cin >> x;
    cout << "Enter 2nd number between 0-1000" << endl;
    cin >> y;
    cout << "X = " << x << endl;
    cout << "Y = " << y << endl;
    x = y;
    y = x;
    cout << "Now " << endl;
    cout << "X is " << x << endl
         << "Y is " << y <<endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
