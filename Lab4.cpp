#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    double meter;
    double mile = 1609.344;
    double conversion;
    cout << "Welcome to conversion tool!" << endl;
    cout << "Press RETURN after entering number" << endl;
    cout << "How many meters? ";
    cin >> meter;
    conversion = meter / mile;
    cout << meter << " meters is equal to " << conversion << " mile(s)!" << endl;
    system("PAUSE");
    return EXIT_SUCCESS;
}
