#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
  double x = 10.76;
  double y = 1.50;
  const int mult = 100;
  
  double dif = x - y;
  cout << dif << endl;
  
  int intDif = static_cast<int>(dif * mult);

  cout << intDif << endl;
  cout << dif * mult << endl;
  
    system("PAUSE");
    return EXIT_SUCCESS;
}
