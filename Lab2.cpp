#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int a = 5;
    int b = 10;
    cout << "a: " << a << " " << "b: " << b << endl;
    a = b;
    b = a;
    cout << "a: " << a << " " << "b: " << b << endl;
    system("PAUSE");
    return EXIT_SUCCESS;
}
