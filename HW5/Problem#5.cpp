#include <cstdlib>
#include <iostream>
//Daniel Canales
//2449175
using namespace std;

int main(int argc, char *argv[])
{
    double liter = .264179;
    double miles;
    double tank;
    double mpg;
    char again;
    do
{
    cout << "Hello, please enter how many liters of gas your" ;
    cout << " car will use" << endl;
    cin >> tank;
    
    cout << "How many miles will your car go? " << endl;
    cin >> miles; 
    
    mpg = miles/(tank/liter);
    cout << "Your car goes " << mpg << " miles per gallon" << endl;
    
    cout << "Try again?" << endl;
    cin >> again;
}   while ( again == 'Y' || again =='y');
    cout << "Done" << endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
