#include <cstdlib>
#include <iostream>
#include <fstream>
//Daniel Canales
//2449175
using namespace std;

int main(int argc, char *argv[])
{
    
    double liter = .264179;
    double mpg;
    
    ifstream inFile;
    int value1, value2;
    inFile.open("data.dat");
    
    cout << "Hello, please enter how many liters of gas your" ;
    cout << " car will use" << endl;
    inFile >> value1;
    cout << value1 << endl;
    cout << "How many miles will your car go? " << endl;
    inFile >> value2;
    inFile.close();
    cout << value2 << endl;;
    mpg = value2/(value1/liter);
    cout << "Your car goes " << mpg << " miles per gallon" << endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
