#include <cstdlib>
#include <iostream>
//Daniel Canales
//2449175
using namespace std;

int main(int argc, char *argv[])
{
    double liter = .264179;
    double miles;
    double tank;
    double mpg;
    double miles2;
    double tank2;
    double mpg2;
    char again;
  
    cout << "Hello, please enter how many liters" ;
    cout << " the 1st car will use?" << endl;
    cin >> tank;
    cout << "How much will the 2nd car use?" << endl;
    cin >> tank2;
    
    cout << "How many miles will the 1st car go? " << endl;
    cin >> miles; 
    cout << "How many miles will the 2nd car go?" << endl;
    cin >> miles2;
    
    mpg = miles/(tank/liter);
    mpg2 = miles2/(tank2/liter);
    cout << "The 1st car goes " << mpg << " miles per gallon" << endl;
    cout << "The 2nd car goes " << mpg2 << " miles per gallon" << endl;
    
    if ( mpg > mpg2)
    {
         cout << "The 1st car is more fuel efficient" << endl;
         }
    else if ( mpg < mpg2)
    {
        cout << "The 2nd car is more fuel efficient" << endl;
        }
    else
    {
        cout << "They are both equal" << endl;
        }
   

    
    system("PAUSE");
    return EXIT_SUCCESS;
}
